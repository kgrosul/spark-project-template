package org.fivt.atp.bigdata.detector

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming._
import org.apache.spark.sql.types._




object KeyWordDetectorMain {
  // Ключевые слова, по которым будем фильтровать
  def isKeyword(arg: String): Boolean = {
    val keyWords = Array("privet", "bonjour", "hello")
    keyWords.contains(arg)
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .getOrCreate()


    val kafkaBrokers = "mipt-node04.atp-fivt.org:9092"
    val inputTopic = "simple-input-had2020005"
    val inputStream = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaBrokers)
      .option("subscribe", inputTopic)
      .option("startingOffsets", "latest")
      .load()


    val kafkaValueColumn = "value"
    val deserializedColumn = "deserialized_value"
    val deserializedStream = inputStream
      .select(col(kafkaValueColumn) cast StringType as deserializedColumn)


    val preparedColumn = "prepared_value"
    val prepareString = udf { string: String =>
      string
        .toLowerCase()
        .filter(char => char.isLetter || char == ' ')
        .split(' ')
    }
    val preparedStream = deserializedStream
      .select(explode(prepareString(col(deserializedColumn))) as preparedColumn)
      .filter(row => isKeyword(row.get(0).toString))
      .select(col(preparedColumn) as kafkaValueColumn)

    val outputTopic = "simple-output-had2020005"
    val checkpointDir = "checkpoints/detector"
    val query = preparedStream.writeStream
      .queryName(this.getClass.getSimpleName)
      .outputMode(OutputMode.Append())
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaBrokers)
      .option("topic", outputTopic)
      .option("checkpointLocation", checkpointDir)
      .start()
    query.awaitTermination()
  }

}
